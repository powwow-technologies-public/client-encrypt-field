/**
 * Created by syntiro on 13/09/2017.
 */
import {ICallback} from "./model/ICallback";
import {ReturnData} from "./model/ReturnData";
import {ReturnDataType} from "./enum/ReturnDataType";
import {FieldEncryptionManager} from "./manager/FieldEncryptionManager";

/**
 * Encrypt field data and return encrypted data
 * via callback E.G {type: 'encrypted-string', data: 'iihkwqhury8473y4r3y3443yr3'}
 * @param fieldData
 * @param fieldType
 * @param secretKey
 * @param callback
 */
export function encryptField(fieldData: any, fieldType: string, secretKey: string, callback: ICallback<ReturnData>){
    try{
        new FieldEncryptionManager().doFieldEncryption(fieldData, fieldType, secretKey, function (returnData: ReturnData) {
            callback(returnData);
        });
    }catch(err){
        callback(new ReturnData(ReturnDataType.toString(ReturnDataType.ERROR), err));
    }
}
