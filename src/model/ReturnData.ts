/**
 * Created by syntiro on 19/09/2017.
 */

export class ReturnData{

    private type: string;
    private data: string;

    public constructor(type: string, data: string){
        this.type = type;
        this.data = data;
    }

    

}
