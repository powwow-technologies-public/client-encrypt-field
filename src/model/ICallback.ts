/**
 * Created by syntiro on 04/05/2017.
 */
export interface ICallback<T> {
    (argument: T) : void
}