/**
 * Created by syntiro on 13/09/2017.
 */
declare module 'file-encryptor'{
    function encryptFile(origFilename: string, encryptedFilename: string, key: string, options: any, callback: any): any;
}