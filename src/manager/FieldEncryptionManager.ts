import {FieldType} from "../enum/FieldType";
import {FileFieldEncryptorDelegate} from "../delegate/FileFieldEncryptorDelegate";
import {ICallback} from "../model/ICallback";
import {ReturnData} from "../model/ReturnData";
import {TextFieldEncryptorDelegate} from "../delegate/TextFieldEncryptorDelegate";
/**
 * Created by syntiro on 26/09/2017.
 */
export class FieldEncryptionManager{

    public doFieldEncryption(fieldData: any, fieldType: string, secretKey: string, callback: ICallback<ReturnData>){
        switch(fieldType.toUpperCase()) {
            case FieldType.toString(FieldType.FILE):
                new FileFieldEncryptorDelegate(fieldData, secretKey).encryptField(function (returnData: ReturnData) {
                    callback(returnData);
                });
                break;
            case FieldType.toString(FieldType.TEXT):
            case FieldType.toString(FieldType.TEXTAREA):
                new TextFieldEncryptorDelegate(fieldData, secretKey).encryptField(function (returnData: ReturnData) {
                    callback(returnData);
                });
                break;
        }
    }
}
