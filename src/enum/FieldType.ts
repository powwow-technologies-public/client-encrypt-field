/**
 * Created by syntiro on 26/09/2017.
 */
export enum FieldType{
    FILE=0,
    TEXT=1,
    DATE=2,
    TEXTAREA=3
}

export namespace FieldType {

    export function toString(fieldType: FieldType): string {
        return FieldType[fieldType];
    }

    export function isFile(fieldType: string){
        return FieldType.isType(fieldType, FieldType.FILE);
    }

    export function isText(fieldType: string){
        return FieldType.isType(fieldType, FieldType.TEXT);
    }

    export function isDate(fieldType: string){
        return FieldType.isType(fieldType, FieldType.DATE);
    }

    export function isTextArea(fieldType: string){
        return FieldType.isType(fieldType, FieldType.TEXTAREA);
    }

    export function isType(fieldTypeStr: string, fieldType: FieldType){
        return FieldType.toString(fieldType) === fieldTypeStr.toUpperCase();
    }

}