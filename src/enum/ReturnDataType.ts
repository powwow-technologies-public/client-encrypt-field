/**
 * Created by syntiro on 26/09/2017.
 */
export enum ReturnDataType{

    ENCRYPTED_STRING=0,
    ERROR=1

}

export namespace ReturnDataType {

    export function toString(returnDataType: ReturnDataType): string {
        return ReturnDataType[returnDataType];
    }
}