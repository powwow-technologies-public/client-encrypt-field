import {ICallback} from "../model/ICallback";
import {ReturnData} from "../model/ReturnData";
/**
 * Created by syntiro on 26/09/2017.
 */
export abstract class FieldEncryptorDelegate{

    protected fieldData: any;
    protected secretKey: string;

    public constructor(fieldData: any, secretKey: string){
        this.fieldData = fieldData;
        this.secretKey = secretKey;
    }

    public abstract encryptField(callback: ICallback<ReturnData>): void;

    public getEncryptedValue(nonEncryptedValue: string): string{
        let encryptor = require('simple-encryptor')(this.secretKey);
        return encryptor.encrypt(nonEncryptedValue);
    }

}
