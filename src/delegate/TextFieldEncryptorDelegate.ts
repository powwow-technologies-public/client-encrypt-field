import {FieldEncryptorDelegate} from "./FieldEncryptorDelegate";
import {ICallback} from "../model/ICallback";
import {ReturnData} from "../model/ReturnData";
import {ReturnDataType} from "../enum/ReturnDataType";
/**
 * Created by johnnybrady on 27/09/2017.
 */

export class TextFieldEncryptorDelegate extends FieldEncryptorDelegate{

    public constructor(fieldData: string, secretKey: string){
        super(fieldData, secretKey);
    }

    public encryptField(callback: ICallback<ReturnData>): void {
        callback(new ReturnData(ReturnDataType.toString(ReturnDataType.ENCRYPTED_STRING), this.getEncryptedValue(this.fieldData)));
    }
}
