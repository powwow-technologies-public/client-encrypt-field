import {FieldEncryptorDelegate} from "./FieldEncryptorDelegate";
import window = require('node-window');
import {ICallback} from "../model/ICallback";
import {ReturnData} from "../model/ReturnData";
import {ReturnDataType} from "../enum/ReturnDataType";
/**
 * Created by syntiro on 26/09/2017.
 */
export class FileFieldEncryptorDelegate extends FieldEncryptorDelegate{


    public constructor(fieldData: Blob, secretKey: string){
        super(fieldData, secretKey);
    }

    public encryptField(callback: ICallback<ReturnData>): void {
        let self = this;
        // Check for the various File API support.
        if (!window.File || !window || !window.FileList || !window.Blob) {
            callback(new ReturnData(ReturnDataType.toString(ReturnDataType.ERROR), 'File, File Reader, FileList or Blob is not supported!'));
            return;
        }
        let reader = new FileReader();
        reader.readAsDataURL(this.fieldData);
        reader.onload = function() {
            callback(new ReturnData(ReturnDataType.toString(ReturnDataType.ENCRYPTED_STRING), self.getEncryptedValue(reader.result)));
            return;
        };
        reader.onerror = function (error) {
            callback(new ReturnData(ReturnDataType.toString(ReturnDataType.ERROR), error.message));
            return;
        };
    }

}
