# client-encrypt-field

A client side form field encryptor for Node.js. 

# Installation

Add it to your node.js project via:

```
npm install client-encrypt-field --save
```

# Usage

As this is a client side script you'll need to compile this module and it's dependencies using browserify or something similar
```js
browserify node_modules/client-encrypt-field/dist/index.js --s clientEncryptFieldModule -o bundle.js

```

To encrypt a file upload:

```js
//Add change event to file upload field
document.addEventListener('DOMContentLoaded',function() {
    //File upload input field
    document.querySelector('input[name="input_4"]').onchange=changeEventHandler;
},false);

//Handle the change event and call encryptField
//Encrypt file blob with secret key 'longAndRandomSecretKey' and return encrypted response data
//via callback E.G {type: 'encrypted-string', data: 'iihkwqhury8473y4r3y3443yr3'} 
function changeEventHandler(event) {
    if (typeof clientEncryptFieldModule !== 'undefined' && clientEncryptFieldModule !== null) {
        clientEncryptFieldModule.encryptField(event.target.files[0], event.target.type, 'longAndRandomSecretKey', function(response){
          console.log(response.type);
          console.log(response.data);
        });
    }
    
}

```

To encrypt text field input:

```js
//Add change event to file upload field
document.addEventListener('DOMContentLoaded',function() {
    //Text input field
    document.querySelector('input[name="input_5"]').onblur=blurEventHandler;
},false);

//Handle the blur event and call encryptField
//Encrypt field text with secret key 'longAndRandomSecretKey' and return encrypted response data
//via callback E.G {type: 'encrypted-string', data: 'iihkwqhury8473y4r3y3443yr3'} 
function blurEventHandler(event) {
    if (typeof clientEncryptFieldModule !== 'undefined' && clientEncryptFieldModule !== null) {
        clientEncryptFieldModule.encryptField(event.target.value, event.target.type, 'longAndRandomSecretKey', function(response){
          console.log(response.type);
          console.log(response.data);
        });
    }
}

```
